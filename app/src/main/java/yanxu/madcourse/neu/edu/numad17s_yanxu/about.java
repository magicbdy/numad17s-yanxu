package yanxu.madcourse.neu.edu.numad17s_yanxu;

/**
 * Created by yanxu on 1/20/17.
 */

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class about extends AppCompatActivity {
    public void setActionBar(String heading) {
        ActionBar bar = getSupportActionBar();
        bar.setTitle(heading);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
        setActionBar("About");
    }
}
