package yanxu.madcourse.neu.edu.numad17s_yanxu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    public Button about;
    public Button generateError;
    public Button badOne;

    public void init() {
        about = (Button) findViewById(R.id.about);
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent click1 = new Intent(MainActivity.this, about.class);
                startActivity(click1);
            }
        }) ;
        generateError = (Button) findViewById(R.id.generate);
        generateError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                badOne.clearAnimation();
            }
        });

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

    }

    public void buttonOnClick(View v) {
        Button button = (Button) v;
    }
}
